# README #

Web-based eVoting System using CodeIgniter PHP Framework.

# Configuration #

* Download the file and extract to directory c:\xampp\htdocs\e-voting or to wherever your apache directory is located. 
* Create "evoting" database using MySQL db. 
* No need to create database tables because it will be instantly created once you access the system.
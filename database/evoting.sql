/*
Navicat MySQL Data Transfer

Source Server         : Local DB
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : evoting

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-07-10 22:10:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_candidates`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_candidates`;
CREATE TABLE `tbl_candidates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` varchar(20) NOT NULL,
  `position_id` int(11) NOT NULL,
  `partylist_id` int(11) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_candidates
-- ----------------------------
INSERT INTO `tbl_candidates` VALUES ('1', '12345-2014', '1', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('2', '12346-2014', '1', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('3', '12347-2014', '1', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('4', '12348-2014', '1', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('5', '12349-2014', '1', '1', '0');
INSERT INTO `tbl_candidates` VALUES ('6', '12350-2014', '1', '1', '0');

-- ----------------------------
-- Table structure for `tbl_courses`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_courses`;
CREATE TABLE `tbl_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `department_id` int(11) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_courses
-- ----------------------------
INSERT INTO `tbl_courses` VALUES ('1', 'BSIT', 'Bachelor of Science in Information Technology', '1', '0');
INSERT INTO `tbl_courses` VALUES ('2', 'BSCS', 'Bachelor of Science in Computer Science', '1', '0');

-- ----------------------------
-- Table structure for `tbl_departments`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_departments`;
CREATE TABLE `tbl_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_departments
-- ----------------------------
INSERT INTO `tbl_departments` VALUES ('1', 'CIT', 'College of Information Technology', '0');

-- ----------------------------
-- Table structure for `tbl_partylists`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_partylists`;
CREATE TABLE `tbl_partylists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_partylists
-- ----------------------------
INSERT INTO `tbl_partylists` VALUES ('1', 'Independent', 'Independent Party', '0');

-- ----------------------------
-- Table structure for `tbl_persons`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_persons`;
CREATE TABLE `tbl_persons` (
  `id` varchar(11) NOT NULL,
  `f_name` varchar(200) NOT NULL,
  `m_name` varchar(200) NOT NULL,
  `l_name` varchar(200) NOT NULL,
  `course_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_persons
-- ----------------------------
INSERT INTO `tbl_persons` VALUES ('12345-2014', 'Raymond', 'Pendulas', 'Baguio', '1', '3', '1', '0');
INSERT INTO `tbl_persons` VALUES ('12346-2014', 'Juan', 'Dela', 'Cruz', '1', '3', '1', '0');
INSERT INTO `tbl_persons` VALUES ('12347-2014', 'Jose', 'Protacio', 'Rizal', '1', '3', '1', '0');
INSERT INTO `tbl_persons` VALUES ('12348-2014', 'Person1', '', 'Person1', '2', '3', '1', '0');
INSERT INTO `tbl_persons` VALUES ('12349-2014', 'Person2', '', 'Person2', '2', '3', '1', '0');
INSERT INTO `tbl_persons` VALUES ('12350-2014', 'Person3', '', 'Person3', '2', '3', '1', '0');

-- ----------------------------
-- Table structure for `tbl_positions`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_positions`;
CREATE TABLE `tbl_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `max_vote` int(11) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_positions
-- ----------------------------
INSERT INTO `tbl_positions` VALUES ('1', 'President', '1', '0');
INSERT INTO `tbl_positions` VALUES ('2', 'Vice-President', '1', '0');
INSERT INTO `tbl_positions` VALUES ('3', 'Secretary', '1', '0');
INSERT INTO `tbl_positions` VALUES ('4', 'Senator', '14', '0');
INSERT INTO `tbl_positions` VALUES ('5', 'Representative', '4', '0');

-- ----------------------------
-- Table structure for `tbl_roles`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_roles`;
CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `deleted_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_roles
-- ----------------------------
INSERT INTO `tbl_roles` VALUES ('1', 'Administrator', '0');
INSERT INTO `tbl_roles` VALUES ('2', 'Chairman', '0');
INSERT INTO `tbl_roles` VALUES ('3', 'Voter', '0');

-- ----------------------------
-- Table structure for `tbl_role_privileges`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_role_privileges`;
CREATE TABLE `tbl_role_privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_role_privileges
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_settings`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_settings`;
CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) NOT NULL,
  `site_slogan` varchar(255) NOT NULL,
  `copyright` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_settings
-- ----------------------------
INSERT INTO `tbl_settings` VALUES ('1', 'SSC ELECTION 2014', 'Supreme Student Council', '<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png\" /></a><br /><span xmlns:dct=\"http://purl.org/dc/terms/\" property=\"dct:title\">e-Voting System</span> by <span xmlns:cc=\"http://creativecommons.org/ns#\" property=\"cc:attributionName\">rpbaguio</span> is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/4.0/\">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.');

-- ----------------------------
-- Table structure for `tbl_tally`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tally`;
CREATE TABLE `tbl_tally` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` varchar(20) NOT NULL,
  `candidate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_tally
-- ----------------------------

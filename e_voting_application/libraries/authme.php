<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Authme {

    private $CI;

    public function __construct() {
        $this->CI = & get_instance();

        $this->CI->load->database();
        $this->CI->load->library('session');
        $this->CI->load->model('persons_model');
        $this->CI->config->load('db_tbl_config');
    }

    public function logged_in() {
        return $this->CI->session->userdata('logged_in');
    }

    public function signin($person_id) {
        $person = $this->CI->persons_model->_get_person_by_id($person_id);
        if ($person) {
            //unset($person->id);
            $this->CI->session->set_userdata(array(
                'logged_in' => true,
                'person' => $person
            ));
            $this->CI->persons_model->_update_person($person->id, array(
                'status' => 1
            ));
            return true;
        }
        return false;
    }

    public function signout($redirect = false) {
        $this->CI->session->sess_destroy();
        if ($redirect) {
            $this->CI->load->helper('url');
            redirect($redirect, 'refresh');
        }
    }

}

/* End of file: authme.php */
/* Location: application/libraries/authme.php */
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

$config['db_tbl_roles'] = 'tbl_roles';
$config['db_tbl_role_privileges'] = 'tbl_role_privileges';
$config['db_tbl_persons'] = 'tbl_persons';
$config['db_tbl_candidates'] = 'tbl_candidates';
$config['db_tbl_positions'] = 'tbl_positions';
$config['db_tbl_tally'] = 'tbl_tally';
$config['db_tbl_courses'] = 'tbl_courses';
$config['db_tbl_departments'] = 'tbl_departments';
$config['db_tbl_partylists'] = 'tbl_partylists';
$config['db_tbl_settings'] = 'tbl_settings';
$config['db_tbl_sessions'] = 'tbl_sessions';

/* End of file: db_tbl_config.php */
/* Location: application/config/db_tbl_config.php */
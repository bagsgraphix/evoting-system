<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered display" width="100%">
                    <?php if ($candidates): ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Candidate</th>
                                <th>Position</th>
                                <th>Party List</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($candidates as $row): ?>
                                <tr>
                                    <td><a href="<?= base_url('candidate') . '/' . $row->id ?>"><?= $row->person_id ?></a></td>
                                    <td><?= $row->l_name ?>,&nbsp;<?= $row->f_name ?></td>
                                    <td><?= $row->position ?></td>
                                    <td><?= $row->party ?></td>
                                    <td>
                                        <div class="btn-group pull-right">
                                            <a href="<?= base_url('candidate') . '/' . $row->id ?>" class="btn btn-xs btn-default btn-lowercase">View</a>
                                            <a href="<?= base_url('candidate/update') . '/' . $row->id ?>" class="btn btn-xs btn-default btn-lowercase">Update</a>
                                            <a href="<?= base_url('candidate/delete') . '/' . $row->id ?>" class="btn btn-xs btn-danger btn-lowercase"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No candidates found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</div>
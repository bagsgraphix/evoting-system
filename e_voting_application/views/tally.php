<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered display" width="100%">
                    <?php if ($tally): ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Voter</th>
                                <th>Candidate</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($tally as $row): ?>
                                <tr>
                                    <td><a href="<?= base_url('tally') . '/' . $row->id ?>"><?= $row->id ?></a></td>
                                    <td><?= $row->l_name ?>,&nbsp;<?= $row->f_name ?></td>
                                    <td><?= $row->candidate ?></td>
                                    <td>
                                        <div class="btn-group pull-right">
                                            <a href="<?= base_url('tally') . '/' . $row->id ?>" class="btn btn-xs btn-default">View</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No tally found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</div>
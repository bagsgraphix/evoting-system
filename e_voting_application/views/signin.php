<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <?php echo form_open(); ?>
                <?php if ($error): ?>
                    <?= $error ?>
                <?php endif; ?>
                <?php echo validation_errors(); ?>
                <div class = "well well-sm">
                    <?php echo (form_error('person_id')) ? '<div class = "form-group has-error has-feedback">' : '<div class = "form-group">'; ?>
                    <?php echo form_label('Person ID', 'person_id'); ?>
                    <?php echo form_input(array('class' => 'form-control input-lg', 'name' => 'person_id', 'placeholder' => 'Please type your identification number here.', 'tabindex' => '1', 'value' => set_value('person_id'))); ?>
                    <?= '</div>' ?>
                </div>
                <button type = "submit" id = "btn-change-state" class = "btn btn-lg btn-primary btn-block btn-animate btn-chunky btn-uppercase up-top down-below" data-loading-text = "loading..." tabindex="2">confirm</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

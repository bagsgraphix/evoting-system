<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2 class="text-center"><?= $page_title ?></h2>
                <hr class="hr-bottom" />
            </div>
        </div>
    </div>
</div>

<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered display" width="100%">
                    <?php if ($positions): ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Position</th>
                                <th>Maximum Vote</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($positions as $row): ?>
                                <tr>
                                    <td><a href="<?= base_url('position') . '/' . $row->id ?>"><?= $row->id ?></a></td>
                                    <td><?= $row->name ?></td>
                                    <td><?= $row->limit ?></td>
                                    <td>
                                        <div class="btn-group pull-right">
                                            <a href="<?= base_url('position') . '/' . $row->id ?>" class="btn btn-xs btn-default">View</a>
                                            <a href="<?= base_url('position/update') . '/' . $row->id ?>" class="btn btn-xs btn-default">Update</a>
                                            <a href="<?= base_url('position/delete') . '/' . $row->id ?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No positions found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered persons" width="100%">
                    <?php if ($persons): ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Course</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($persons as $row): ?>
                                <tr>
                                    <td><a href="<?= base_url('person') . '/' . $row->id ?>"><?= $row->id ?></a></td>
                                    <td><?= $row->f_name ?></td>
                                    <td><?= $row->l_name ?></td>
                                    <td><?= $row->course ?></td>
                                    <td><?= $row->role ?></td>
                                    <td><?= $row->status ?></td>
                                    <td>
                                        <div class="btn-group pull-right">
                                            <a href="<?= base_url('person') . '/' . $row->id ?>" class="btn btn-xs btn-default">View</a>
                                            <a href="<?= base_url('person/update') . '/' . $row->id ?>" class="btn btn-xs btn-default">Update</a>
                                            <a href="<?= base_url('person/delete') . '/' . $row->id ?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No persons found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <?php echo form_open('ballot'); ?>
            <div class="col-md-12 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <div class="page-header">
                    <h1><?= site_title() ?></h1>
                </div>
                <?php if (position()): ?>
                    <?php foreach (position() as $row): ?> 
                        <h2><?= $row->name ?><small>&nbsp;&mdash;&nbsp;Vote for <?= convert_number_to_words(max_vote($row->id)) . nbs() . '(' . max_vote($row->id) . ')' ?></small></h2>
                        <hr class="hr-bottom" />
                        <?php echo validation_errors(); ?>
                        <?php $candidates = candidates($row->id); ?>
                        <?php $max_vote = max_vote($row->id); ?>
                        <?php $position = $row->name; ?>
                        <?php if ($candidates): ?>
                            <div class="row">
                                <?php foreach ($candidates as $row): ?>    
                                    <div class="col-md-4">
                                        <div class="ballot-form <?= $position . '_' . $max_vote ?>">
                                            <input id="<?= $position . '_' . $max_vote ?>" data-type="max" type="hidden" value="<?= $max_vote ?>">
                                            <input type="checkbox" id="candidate_<?= $row->id ?>" class="css-checkbox" name="candidate_id[]" value="<?= $row->id ?>"<?php echo set_checkbox('candidate_id', $row->id); ?> />
                                            <label for="candidate_<?= $row->id ?>" class="css-label">
                                                <img class="img-rounded pull-right" src="holder.js/32x32/sky">
                                                <?= $row->f_name . nbs() . $row->l_name . br() . '<small>' . $row->party . '</small>' ?>
                                            </label>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php else: ?>
                            <p class="text text-danger">No candidates found.</p>
                        <?php endif; ?> 
                    <?php endforeach; ?>
                <?php else: ?>
                    <p class="text text-danger">No positions found.</p>
                <?php endif; ?> 
                <div class="up-top down-below">
                    <button type = "submit" id = "btn-change-state" class = "btn btn-lg btn-primary btn-animate btn-chunky btn-uppercase" data-loading-text = "loading...">vote</button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
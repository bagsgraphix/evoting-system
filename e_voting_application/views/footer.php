<footer>
    <div class="container">
        <p class="text-center"><?= site_copyright() ?></p>
    </div>
</footer>

<!-- Loading JS Libraries -->
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/jquery-2.1.0.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/holder.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/modernizr-2.7.2.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/jquery.timeago.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/tinymce/js/tinymce/tinymce.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/pace.min.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/datatables/js/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/datatables/js/dataTables.bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('e_voting_assets/scripts/custom.init.js'); ?>"></script>

<script type="text/javascript">
    $(function() {
        // Holder.js
        Holder.add_theme("bright", {background: "#EEE", foreground: "#777", size: 30});
        // Timeago
        $("time").timeago();
        // Tooltip
        $('body').tooltip({
            selector: '[data-tooltip=tooltip]',
            container: 'body'
        });
        // Popover
        $("#popover").popover({
            trigger: "hover",
            html: true,
            content: function() {
                return;
            }
        });
        // Tinymce WYSIWYG
        tinymce.init({
            selector: "textarea",
            skin: "lightgray",
            theme: "modern",
            menubar: false, // Disable all menus
            statusbar: true, // Disable status bar
            toolbar_items_size: 'medium',
            forced_root_block: false, // Remove <p> tag
            force_p_newlines: false,
            remove_linebreaks: false,
            force_br_newlines: false,
            remove_trailing_nbsp: false,
            verify_html: false,
            relative_urls: true,
            plugins: [
                "wordcount",
                "link",
                "advlist",
                "autolink",
                "preview",
                "code"
            ],
            toolbar: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist | link unlink | preview code"
        });
        // Prevent bootstrap dialog from blocking focusin
        $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });
        // Feedback Message
        $('#feedback').animate({
            right: '+=75'
        }, 500).animate({
            right: '-=75'
        }, 500).fadeOut(3000);
        // Datatables      
        $('table.persons').dataTable({
            "order": [[0, "asc"]]
        });
        $('table.display').dataTable({
            "order": [[0, "asc"]]
        });
        // Maximum number of checkbox selected
        var data = [];
        $('input[data-type="max"]').each(function() {
            data.push({
                id: $(this).attr("id"),
                value: $(this).attr("value")
            });
        });
        $.each(data, function() {
            var pos = this.id;
            var max = this.value;
            var checkboxes = $('.' + pos + ' ' + 'input[type="checkbox"]');
            checkboxes.change(function() {
                var current = checkboxes.filter(':checked').length;
                checkboxes.filter(':not(:checked)').prop('disabled', current >= max);
            });
        });
    });
</script>

</body>
</html>
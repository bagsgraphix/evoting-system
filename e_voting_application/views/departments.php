<div id="mainWrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 white-bg box-shadow-wide up-top" style="padding: 10px 20px;">
                <h2><?= $page_title ?></h2>
                <hr class="hr-bottom" />
                <table class="table table-striped table-bordered display" width="100%">
                    <?php if ($departments): ?>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Dept.</th>
                                <th>Description</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($departments as $row): ?>
                                <tr>
                                    <td><a href="<?= base_url('department') . '/' . $row->id ?>"><?= $row->id ?></a></td>
                                    <td><?= $row->name ?></td>
                                    <td><?= $row->description ?></td>
                                    <td>
                                        <div class="btn-group pull-right">
                                            <a href="<?= base_url('department') . '/' . $row->id ?>" class="btn btn-xs btn-default">View</a>
                                            <a href="<?= base_url('department/update') . '/' . $row->id ?>" class="btn btn-xs btn-default">Update</a>
                                            <a href="<?= base_url('department/delete') . '/' . $row->id ?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    <?php else: ?>
                        <tbody>
                            <tr>
                                <td colspan="3">No departments found.</td>
                            </tr>
                        </tbody>
                    <?php endif; ?>
                </table>
            </div>
        </div>
    </div>
</div>
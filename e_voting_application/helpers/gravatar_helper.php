<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
Usage:

1) Copy gravatar_helper.php to your application/helpers folder
2) Load helper in controller or view
    $this->load->helper('gravatar');
3) Call the function in view:
    <?=gravatar('youremail@yourdomain.com', '80', 'default', 'g', 'gravatar'); ?>

param string $email - The email address
param string $size - Size in pixels, defaults to 80px [ 1 - 512 ]
param string $default_image - Default imageset to use [ default | 404 | mm | identicon | monsterid | wavatar ]
param string $rating - Maximum rating (inclusive) [ g | pg | r | x ]
param string $class - CSS class name assigned to image

https://github.com/johanson/Codeigniter-Gravatar-helper

*/

if (!function_exists('gravatar')) {

    function gravatar($email, $size = null, $default_image = null, $rating = null, $class = '') {

	$gravatar_url = 'http://www.gravatar.com/avatar/';
    	$gravatar_url .= md5(strtolower(trim($email)));
	$gravatar_url .= "?s=$size&d=$default_image&r=$rating";

	if ($class !== '') {
		$class = 'class="'.$class.'"';
	}

        return '<img src='.$gravatar_url.' alt="gravatar" width="'.$size.'" height="'.$size.'" '.$class.'>';

    }
}
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Role_Privileges_Model extends CI_Model {

    public $tbl;

    public function __construct() {
        parent::__construct();

        $this->config->load('db_tbl_config');
        $this->tbl = $this->config->item('db_tbl_role_privileges');

        if (!$this->db->table_exists($this->tbl)) {
            $this->_create_tbl_role_privileges();
        }
    }

    public function _create_tbl_role_privileges() {
        $this->load->dbforge();
        $this->db->query('SET storage_engine=MYISAM;');
        $this->dbforge->add_field('id INT(11) AUTO_INCREMENT NOT NULL');
        $this->dbforge->add_field('role_id INT(11) NOT NULL');
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl);
    }

}

/* 
 * end of file 
 * location: models/role_privileges_model.php 
 */
    
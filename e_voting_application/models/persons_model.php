<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Persons_Model extends CI_Model {

    public $tbl;

    public function __construct() {
        parent::__construct();

        $this->config->load('db_tbl_config');
        $this->tbl = $this->config->item('db_tbl_persons');

        if (!$this->db->table_exists($this->tbl)) {
            $this->_create_tbl_persons();
        }
    }

    public function _get_persons() {
        $this->db->select('tbl_persons.id, '
                . 'tbl_persons.f_name,'
                . 'tbl_persons.l_name,'
                . 'tbl_persons.status,'
                . 'tbl_courses.name AS course, '
                . 'tbl_roles.name AS role');
        $this->db->from($this->tbl);
        $this->db->join('tbl_courses', 'tbl_courses.id = tbl_persons.course_id');
        $this->db->join('tbl_roles', 'tbl_roles.id = tbl_persons.role_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function _get_person($id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function _get_person_by_id($id) {
        $query = $this->db->get_where($this->tbl, array('id' => $id));
        return ($query->num_rows()) ? $query->row() : false;
    }

    public function _get_person_by_status($status) {
        $query = $this->db->get_where($this->tbl, array('status' => $status));
        return ($query->num_rows()) ? $query->row() : false;
    }

    public function _update_person($id, $data) {
        $this->db->where('id', $id);
        $this->db->update($this->tbl, $data);
    }

    public function _create_tbl_persons() {
        $this->load->dbforge();
        $this->db->query('SET storage_engine=MYISAM;');
        $this->dbforge->add_field('id VARCHAR(11) NOT NULL');
        $this->dbforge->add_field('f_name VARCHAR(200) NOT NULL');
        $this->dbforge->add_field('m_name VARCHAR(200) NOT NULL');
        $this->dbforge->add_field('l_name VARCHAR(200) NOT NULL');
        $this->dbforge->add_field('course_id INT(11) NOT NULL');
        $this->dbforge->add_field('role_id INT(11) NOT NULL');
        $this->dbforge->add_field('status INT(11) DEFAULT 1 NOT NULL');
        $this->dbforge->add_field('deleted_flag INT(1) DEFAULT 0 NOT NULL');
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl);
    }

}

/* 
 * end of file 
 * location: models/persons_model.php 
 */
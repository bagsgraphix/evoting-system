<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Candidates_Model extends CI_Model {

    public $tbl;

    public function __construct() {
        parent::__construct();

        $this->config->load('db_tbl_config');
        $this->tbl = $this->config->item('db_tbl_candidates');

        if (!$this->db->table_exists($this->tbl)) {
            $this->_create_tbl_candidates();
        }
    }

    public function _get_candidates() {
        $this->db->select('tbl_candidates.id, '
                . 'tbl_candidates.person_id,'
                . 'tbl_persons.f_name,'
                . 'tbl_persons.l_name,'
                . 'tbl_positions.name AS position,'
                . 'tbl_partylists.name AS party');
        $this->db->from($this->tbl);
        $this->db->join('tbl_persons', 'tbl_persons.id = tbl_candidates.person_id');
        $this->db->join('tbl_positions', 'tbl_positions.id = tbl_candidates.position_id');
        $this->db->join('tbl_partylists', 'tbl_partylists.id = tbl_candidates.partylist_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function _get_candidates_by_position($id) {
        $this->db->select('tbl_candidates.id,'
                . 'tbl_candidates.person_id,'
                . 'tbl_persons.f_name,'
                . 'tbl_persons.l_name,'
                . 'tbl_positions.name AS position,'
                . 'tbl_partylists.name AS party');
        $this->db->from($this->tbl);
        $this->db->join('tbl_persons', 'tbl_persons.id = tbl_candidates.person_id');
        $this->db->join('tbl_positions', 'tbl_positions.id = tbl_candidates.position_id');
        $this->db->join('tbl_partylists', 'tbl_partylists.id = tbl_candidates.partylist_id');
        $this->db->where('tbl_candidates.position_id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function _create_tbl_candidates() {
        $this->load->dbforge();
        $this->db->query('SET storage_engine=MYISAM;');
        $this->dbforge->add_field('id INT(11) NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('person_id VARCHAR(20) NOT NULL');
        $this->dbforge->add_field('position_id INT(11) NOT NULL');
        $this->dbforge->add_field('partylist_id INT(11) NOT NULL');
        $this->dbforge->add_field('deleted_flag INT(1) DEFAULT 0 NOT NULL');
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl);
    }

}

/* 
 * end of file 
 * location: models/candidates_model.php 
 */
    
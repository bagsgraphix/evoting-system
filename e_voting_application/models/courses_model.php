<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Courses_Model extends CI_Model {

    public $tbl;

    public function __construct() {
        parent::__construct();

        $this->config->load('db_tbl_config');
        $this->tbl = $this->config->item('db_tbl_courses');

        if (!$this->db->table_exists($this->tbl)) {
            $this->_create_tbl_courses();
        }
    }

    public function _get_courses() {
        $this->db->select('tbl_courses.id, '
                . 'tbl_courses.name AS course,'
                . 'tbl_courses.description,'
                . 'tbl_departments.name AS department');
        $this->db->from($this->tbl);
        $this->db->join('tbl_departments', 'tbl_departments.id = tbl_courses.department_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function _get_course($id) {
        $this->db->select('*');
        $this->db->from($this->tbl);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    public function _create_tbl_courses() {
        $this->load->dbforge();
        $this->db->query('SET storage_engine=MYISAM;');
        $this->dbforge->add_field('id INT(11) NOT NULL AUTO_INCREMENT');
        $this->dbforge->add_field('name VARCHAR(200) NOT NULL');
        $this->dbforge->add_field('description VARCHAR(200) NOT NULL');
        $this->dbforge->add_field('department_id INT(11) NOT NULL');
        $this->dbforge->add_field('deleted_flag INT(1) DEFAULT 0 NOT NULL');
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl);
    }

}

/* 
 * end of file 
 * location: models/courses_model.php 
 */
    
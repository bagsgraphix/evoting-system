<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Roles extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);   
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Roles',
            'roles' => $this->roles_model->_get_roles()
        );
        $this->load->view('header', $obj);
        $this->load->view('roles');
        $this->load->view('footer');
    }

}

/* 
 * end of file 
 * location: controllers/roles.php 
 */

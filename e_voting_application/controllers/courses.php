<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Courses extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Courses',
            'courses' => $this->courses_model->_get_courses()
        );
        $this->load->view('header', $obj);
        $this->load->view('courses');
        $this->load->view('footer');
    }

}

/* 
 * end of file 
 * location: controllers/courses.php 
 */
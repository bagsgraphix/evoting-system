<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Positions extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Positions',
            'positions' => $this->positions_model->_get_positions()
        );
        $this->load->view('header', $obj);
        $this->load->view('positions');
        $this->load->view('footer');
    }

}

/* 
 * end of file 
 * location: controllers/positions.php 
 */
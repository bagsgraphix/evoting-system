<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Candidates extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Candidates',
            'candidates' => $this->candidates_model->_get_candidates()
        );
        $this->load->view('header', $obj);
        $this->load->view('candidates');
        $this->load->view('footer');
    }

}

/* 
 * end of file 
 * location: controllers/candidates.php 
 */

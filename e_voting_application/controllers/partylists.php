<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Partylists extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Party Lists',
            'partylists' => $this->partylists_model->_get_partylists()
        );
        $this->load->view('header', $obj);
        $this->load->view('partylists');
        $this->load->view('footer');
    }

}

/* 
 * end of file 
 * location: controllers/partylists.php 
 */
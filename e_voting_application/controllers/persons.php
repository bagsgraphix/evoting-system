<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Persons extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Persons',
            'persons' => $this->persons_model->_get_persons()
        );
        $this->load->view('header', $obj);
        $this->load->view('persons');
        $this->load->view('footer');
    }

}

/* 
 * end of file 
 * location: controllers/persons.php 
 */

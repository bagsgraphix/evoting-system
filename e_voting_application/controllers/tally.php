<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tally extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        $obj = array(
            'page_title' => 'Manage Tally',
            'tally' => $this->tally_model->_get_tally()
        );
        $this->load->view('header', $obj);
        $this->load->view('tally');
        $this->load->view('footer');
    }

}

/* 
 * end of file 
 * location: controllers/tally.php 
 */
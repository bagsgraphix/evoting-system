<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ballot extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->output->enable_profiler(FALSE);
    }

    public function index() {
        if (!logged_in()) {
            redirect('auth/signin');
        } else {
            $this->ballot();
        }
    }

    public function ballot() {
        if (logged_in()) {
            $this->form_validation->set_rules('candidate_id[]', 'Candidate ID', 'required|callback_checkbox_empty');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            if ($this->form_validation->run()) {
                $this->tally_model->_create_tally();
                $this->signout();
            } else {
                $obj = array(
                    'page_title' => 'Ballot',
                    'positions' => $this->positions_model->_get_positions()
                );
                $this->load->view('header', $obj);
                $this->load->view('ballot');
                $this->load->view('footer');
            }
        } else {
            redirect('auth/signin');
        }
    }

    public function checkbox_empty() {
        if ($this->input->post('candidate_id') === '') {
            $this->form_validation->set_message('checkbox_empty', 'Please select candidates.');
            return false;
        } else {
            return true;
        }
    }

    public function signout() {
        if (!logged_in()) {
            redirect('auth/signin');
        } else {
            $this->authme->signout('success');
        }
    }

}

/*
* end of file 
* location: controllers/ballot.php 
*/    
$(function() {
    /* Back to Top */
    $(window).scroll(function() {
        var up = $(this);
        if (up.scrollTop() !== 0) {
            $('#pageup').fadeIn();
        } else {
            $('#pageup').fadeOut();
        }
    });
    $('#pageup').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });

    /* Tooltip */
    //$('[data-toggle="tooltip"]').tooltip({ 'container': 'body', 'trigger': 'hover', 'placement': 'bottom' });
    $('body').tooltip({
        selector: '[data-tooltip=tooltip]',
        container: 'body'
    });

    /* Nav Tab */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        e.target; // activated tab
        e.relatedTarget; // previous tab
    });

    /* Button Change State */
    $('#btn-change-state').click(function() {
        var btn = $(this);
        btn.button('loading');
        setTimeout(function() {
            btn.button('reset');
        }, 3000);
    });
});